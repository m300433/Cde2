/********************************************************************************
** Form generated from reading UI file 'MainWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.3.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *m_actionOpen;
    QAction *m_actionSave;
    QAction *m_actionSaveAs;
    QAction *m_actionOpenVariable;
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout_6;
    QGridLayout *m_viewLayout;
    QFrame *m_editBarBottom;
    QHBoxLayout *horizontalLayout_3;
    QFrame *m_editBarMinMaxColor;
    QHBoxLayout *horizontalLayout_5;
    QLabel *m_lanbelMinl;
    QLineEdit *m_lineEditMin;
    QLabel *m_labelMax;
    QLineEdit *m_lineEditMax;
    QPushButton *m_ButtonConfirmNewColorRange;
    QSpacerItem *m_editBarBottomSpacer;
    QFrame *m_editBarMovement;
    QHBoxLayout *horizontalLayout_4;
    QLabel *m_labelLon;
    QLineEdit *m_lineEditLat;
    QLabel *m_labelLat;
    QLineEdit *m_lineEditLat_2;
    QMenuBar *m_menubar;
    QMenu *m_menuFile;
    QMenu *m_menuOptions;
    QMenu *m_menuPrint;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(800, 600);
        m_actionOpen = new QAction(MainWindow);
        m_actionOpen->setObjectName(QStringLiteral("m_actionOpen"));
        m_actionSave = new QAction(MainWindow);
        m_actionSave->setObjectName(QStringLiteral("m_actionSave"));
        m_actionSaveAs = new QAction(MainWindow);
        m_actionSaveAs->setObjectName(QStringLiteral("m_actionSaveAs"));
        m_actionOpenVariable = new QAction(MainWindow);
        m_actionOpenVariable->setObjectName(QStringLiteral("m_actionOpenVariable"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        centralwidget->setMouseTracking(true);
        centralwidget->setFocusPolicy(Qt::StrongFocus);
        centralwidget->setAcceptDrops(true);
        verticalLayout_6 = new QVBoxLayout(centralwidget);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        m_viewLayout = new QGridLayout();
        m_viewLayout->setObjectName(QStringLiteral("m_viewLayout"));

        verticalLayout_6->addLayout(m_viewLayout);

        m_editBarBottom = new QFrame(centralwidget);
        m_editBarBottom->setObjectName(QStringLiteral("m_editBarBottom"));
        horizontalLayout_3 = new QHBoxLayout(m_editBarBottom);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        m_editBarMinMaxColor = new QFrame(m_editBarBottom);
        m_editBarMinMaxColor->setObjectName(QStringLiteral("m_editBarMinMaxColor"));
        horizontalLayout_5 = new QHBoxLayout(m_editBarMinMaxColor);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        m_lanbelMinl = new QLabel(m_editBarMinMaxColor);
        m_lanbelMinl->setObjectName(QStringLiteral("m_lanbelMinl"));

        horizontalLayout_5->addWidget(m_lanbelMinl);

        m_lineEditMin = new QLineEdit(m_editBarMinMaxColor);
        m_lineEditMin->setObjectName(QStringLiteral("m_lineEditMin"));

        horizontalLayout_5->addWidget(m_lineEditMin);

        m_labelMax = new QLabel(m_editBarMinMaxColor);
        m_labelMax->setObjectName(QStringLiteral("m_labelMax"));

        horizontalLayout_5->addWidget(m_labelMax);

        m_lineEditMax = new QLineEdit(m_editBarMinMaxColor);
        m_lineEditMax->setObjectName(QStringLiteral("m_lineEditMax"));

        horizontalLayout_5->addWidget(m_lineEditMax);

        m_ButtonConfirmNewColorRange = new QPushButton(m_editBarMinMaxColor);
        m_ButtonConfirmNewColorRange->setObjectName(QStringLiteral("m_ButtonConfirmNewColorRange"));

        horizontalLayout_5->addWidget(m_ButtonConfirmNewColorRange);


        horizontalLayout_3->addWidget(m_editBarMinMaxColor);

        m_editBarBottomSpacer = new QSpacerItem(80, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(m_editBarBottomSpacer);

        m_editBarMovement = new QFrame(m_editBarBottom);
        m_editBarMovement->setObjectName(QStringLiteral("m_editBarMovement"));
        horizontalLayout_4 = new QHBoxLayout(m_editBarMovement);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        m_labelLon = new QLabel(m_editBarMovement);
        m_labelLon->setObjectName(QStringLiteral("m_labelLon"));

        horizontalLayout_4->addWidget(m_labelLon);

        m_lineEditLat = new QLineEdit(m_editBarMovement);
        m_lineEditLat->setObjectName(QStringLiteral("m_lineEditLat"));

        horizontalLayout_4->addWidget(m_lineEditLat);

        m_labelLat = new QLabel(m_editBarMovement);
        m_labelLat->setObjectName(QStringLiteral("m_labelLat"));

        horizontalLayout_4->addWidget(m_labelLat);

        m_lineEditLat_2 = new QLineEdit(m_editBarMovement);
        m_lineEditLat_2->setObjectName(QStringLiteral("m_lineEditLat_2"));

        horizontalLayout_4->addWidget(m_lineEditLat_2);


        horizontalLayout_3->addWidget(m_editBarMovement);


        verticalLayout_6->addWidget(m_editBarBottom, 0, Qt::AlignBottom);

        MainWindow->setCentralWidget(centralwidget);
        m_menubar = new QMenuBar(MainWindow);
        m_menubar->setObjectName(QStringLiteral("m_menubar"));
        m_menubar->setGeometry(QRect(0, 0, 800, 20));
        m_menuFile = new QMenu(m_menubar);
        m_menuFile->setObjectName(QStringLiteral("m_menuFile"));
        m_menuOptions = new QMenu(m_menubar);
        m_menuOptions->setObjectName(QStringLiteral("m_menuOptions"));
        m_menuPrint = new QMenu(m_menubar);
        m_menuPrint->setObjectName(QStringLiteral("m_menuPrint"));
        MainWindow->setMenuBar(m_menubar);

        m_menubar->addAction(m_menuFile->menuAction());
        m_menubar->addAction(m_menuOptions->menuAction());
        m_menubar->addAction(m_menuPrint->menuAction());
        m_menuFile->addAction(m_actionOpen);
        m_menuFile->addAction(m_actionOpenVariable);
        m_menuFile->addAction(m_actionSave);
        m_menuFile->addAction(m_actionSaveAs);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        m_actionOpen->setText(QApplication::translate("MainWindow", "Open", 0));
        m_actionOpen->setShortcut(QApplication::translate("MainWindow", "Ctrl+O", 0));
        m_actionSave->setText(QApplication::translate("MainWindow", "Save", 0));
        m_actionSave->setShortcut(QApplication::translate("MainWindow", "Ctrl+S", 0));
        m_actionSaveAs->setText(QApplication::translate("MainWindow", "Save as", 0));
        m_actionOpenVariable->setText(QApplication::translate("MainWindow", "Open variable", 0));
        m_actionOpenVariable->setShortcut(QApplication::translate("MainWindow", "Ctrl+V", 0));
        m_lanbelMinl->setText(QApplication::translate("MainWindow", "Min", 0));
        m_labelMax->setText(QApplication::translate("MainWindow", "Max", 0));
        m_ButtonConfirmNewColorRange->setText(QApplication::translate("MainWindow", "Confirm Color Range", 0));
        m_labelLon->setText(QApplication::translate("MainWindow", "Lon", 0));
        m_lineEditLat->setText(QApplication::translate("MainWindow", "70", 0));
        m_labelLat->setText(QApplication::translate("MainWindow", "Lat", 0));
        m_lineEditLat_2->setText(QApplication::translate("MainWindow", "70", 0));
        m_menuFile->setTitle(QApplication::translate("MainWindow", "File", 0));
        m_menuOptions->setTitle(QApplication::translate("MainWindow", "Options", 0));
        m_menuPrint->setTitle(QApplication::translate("MainWindow", "Print", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
