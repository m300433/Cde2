/********************************************************************************
** Form generated from reading UI file 'View.ui'
**
** Created by: Qt User Interface Compiler version 5.3.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VIEW_H
#define UI_VIEW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_View
{
public:
    QGridLayout *gridLayout;
    QSlider *m_horizontalSlider;
    QSlider *m_verticalSlider;
    QHBoxLayout *m_topLayout;
    QLabel *m_viewLabel;
    QSpacerItem *horizontalSpacer;
    QLabel *m_connectionLabel;
    QHBoxLayout *m_checkBoxes;

    void setupUi(QWidget *View)
    {
        if (View->objectName().isEmpty())
            View->setObjectName(QStringLiteral("View"));
        View->setWindowModality(Qt::ApplicationModal);
        View->resize(400, 300);
        View->setMouseTracking(true);
        View->setFocusPolicy(Qt::StrongFocus);
        View->setLayoutDirection(Qt::LeftToRight);
        gridLayout = new QGridLayout(View);
        gridLayout->setSpacing(1);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setSizeConstraint(QLayout::SetMaximumSize);
        gridLayout->setContentsMargins(1, 1, 1, 1);
        m_horizontalSlider = new QSlider(View);
        m_horizontalSlider->setObjectName(QStringLiteral("m_horizontalSlider"));
        m_horizontalSlider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(m_horizontalSlider, 4, 0, 1, 1);

        m_verticalSlider = new QSlider(View);
        m_verticalSlider->setObjectName(QStringLiteral("m_verticalSlider"));
        m_verticalSlider->setOrientation(Qt::Vertical);

        gridLayout->addWidget(m_verticalSlider, 3, 1, 1, 1);

        m_topLayout = new QHBoxLayout();
        m_topLayout->setObjectName(QStringLiteral("m_topLayout"));
        m_topLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        m_topLayout->setContentsMargins(0, 0, 0, -1);
        m_viewLabel = new QLabel(View);
        m_viewLabel->setObjectName(QStringLiteral("m_viewLabel"));

        m_topLayout->addWidget(m_viewLabel);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        m_topLayout->addItem(horizontalSpacer);

        m_connectionLabel = new QLabel(View);
        m_connectionLabel->setObjectName(QStringLiteral("m_connectionLabel"));
        m_connectionLabel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        m_topLayout->addWidget(m_connectionLabel);

        m_checkBoxes = new QHBoxLayout();
        m_checkBoxes->setObjectName(QStringLiteral("m_checkBoxes"));

        m_topLayout->addLayout(m_checkBoxes);


        gridLayout->addLayout(m_topLayout, 1, 0, 1, 1);


        retranslateUi(View);

        QMetaObject::connectSlotsByName(View);
    } // setupUi

    void retranslateUi(QWidget *View)
    {
        View->setWindowTitle(QApplication::translate("View", "Form", 0));
        m_viewLabel->setText(QApplication::translate("View", "ViewName", 0));
        m_connectionLabel->setText(QApplication::translate("View", "Connections: ", 0));
    } // retranslateUi

};

namespace Ui {
    class View: public Ui_View {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VIEW_H
