#include "cdePolygonMath.h"

bool
polygonContainsPoint(QPolygonF p_poly, QPointF p_point)
{
  double otherResult;
  double Ax;
  double Bx;
  double Ay;
  double By;
  double x;
  double y;
  for (int i = 0; i < p_poly.size(); i++)
    {
      /// std::cout << "I " << i << std::endl;
      // std::cout << "index: " << (i + 1) % p_poly.size() << std::endl;
      Ax = p_poly[i].x();
      Bx = p_poly[(i + 1) % p_poly.size()].x();
      Ay = p_poly[i].y();
      By = p_poly[(i + 1) % p_poly.size()].y();
      x = p_point.x();
      y = p_point.y();

      otherResult = ((Bx - Ax) * (y - Ay)) - ((By - Ay) * (x - Ax));
      // if point is on line or right of line AB
      if (otherResult > 0)
        {
          // std::cout << "returning false" << std::endl;
          return false;
        }
    }
  // std::cout << "returning true" << std::endl;
  return true;
}

QPointF
getPolygonCenter(QPolygonF &p_polygon)
{
  /*
   * Calculations taken from:
   * https://en.wikipedia.org/wiki/Centroid#Centroid_of_a_polygon
   */
  double surface = 0;
  double centerX = 0;
  double centerY = 0;
  unsigned int ncorners = p_polygon.size();
  double temp;  // (x_i * y_i+1) - (x_i+1 * y_i) // used in every calculation
  int n1, n2;

  for (int i = 0; i < ncorners; i++)
    {
      n1 = i % ncorners;
      n2 = (i + 1) % ncorners;
      temp = (p_polygon[n1].x() * p_polygon[n2].y())
             - (p_polygon[n2].x() * p_polygon[n1].y());
      // calculate surface
      surface += temp;
      // calculate center x
      centerX += (p_polygon[n1].x() + p_polygon[n2].x()) * temp;
      // calculate center Y
      centerY += (p_polygon[n1].y() + p_polygon[n2].y()) * temp;
    }
  surface *= 0.5;
  centerX *= (1.f / (6.f * surface));
  centerY *= (1.f / (6.f * surface));

  return QPointF(centerX, centerY);
}
