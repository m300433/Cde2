/*
  This file is part of CDO. CDO is a collection of Operators to
  manipulate and analyse Climate model Data.

  Copyright (C) 2003-2018 Uwe Schulzweida, <uwe.schulzweida AT mpimet.mpg.de>
  See COPYING file for copying and redistribution conditions.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
*/
#include "debugOutput.h"

constexpr int padding_width = 30;

namespace Debug
{
  void
  SetDebug(int p_debug_level)
  {
  }

  int VIEW = 1;
  int VIEW_EMIT = 1;
  int VIEW_CONNECT = 1;
  int MAIN = 1;
  int MAIN_EMIT = 1;
  int MAIN_CONNECT = 1;
  int MOUSE_EVENT = 1;
  int CHECK_BOX = 1;
  int THREADING = 1;
  int IMAGE_CREATION = 1;
  int SUBSCENE = 1;
  int SCENEMANAGER = 1;

  // File switches and streams
  std::string outfile;
  bool print_to_seperate_file;
  std::fstream outfile_stream;
  std::string
  get_padding(const char *p_func)
  {
    size_t len = strlen(p_func);

    return std::string(padding_width - len, ' ');
  }

  void
  StartMessage()
  {
    std::stringstream message;
    outfile_stream.open(outfile, std::fstream::in | std::fstream::app);

    message << std::string(padding_width, ' ')
            << "  == CDE Start ==" << std::endl;
    printMessage(message);
  }
  void
  EndMessage()
  {
    std::stringstream message;
    message << std::string(padding_width, ' ')
            << "  == CDE End ==" << std::endl;
    printMessage(message);
    outfile_stream.close();
  }

  std::string
  argvToString(int argc, const char **argv)
  {
    std::string input_string = "";
    for (int i = 0; i < argc; i++)
      {
        input_string += argv[i];
        input_string += " ";
      }
    return input_string;
  }
  void
  printError(std::stringstream &p_message, bool printToBoth)
  {
    if (print_to_seperate_file)
      {
        outfile_stream << p_message.str();
      }
    std::cerr << p_message.str();
  }

  void
  printMessage(std::stringstream &p_message, bool printToBoth)
  {
    if (print_to_seperate_file || (print_to_seperate_file && printToBoth))
      {
        outfile_stream << p_message.str();
      }

    if (!print_to_seperate_file || printToBoth)
      {
        std::cout << p_message.str();
      }
  }

  void
  DebugConnect(const char *p_func, int scope, std::string type1, int sourceID,
               std::string type2, int destID, std::string signalName,
               std::string slotName)
  {
    PrintDebug(p_func, scope, "connecting ", type1, " ", sourceID, " to ",
               type1, " ", destID, " with signal ", signalName, " and slot ",
               slotName);
  }
  void
  DebugDisconnect(const char *p_func, int scope, std::string type1,
                  int sourceID, std::string type2, int destID,
                  std::string signalName, std::string slotName)
  {
    PrintDebug(p_func, scope, "disconnecting ", type1, " ", sourceID, " to ",
               type1, " ", destID, " with signal ", signalName, " and slot ",
               slotName);
  }

}  // namespace Debug
namespace Log
{
  void
  StdOut(std::stringstream &p_message)
  {
    std::cout << p_message.str();
  }
}  // namespace Log
