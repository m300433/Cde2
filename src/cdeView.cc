#include "cdeView.h"
#include "cdeCheckBox.h"
#include <iostream>

#include <QMouseEvent>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QLabel>

#include "debugOutput.h"
#include "field.h"

CdeView::CdeView(const QString p_filename, int p_ID, QWidget *p_parent)
    : QDockWidget(p_parent), m_ID(p_ID)
{
  std::cout << "Const:" << p_filename.toStdString() << std::endl;
  m_fileName = p_filename.toStdString();
  std::cout << "Const2 :" << m_fileName << std::endl;
  QWidget *m_centralWidget = new QWidget(this);
  m_ui.setupUi(m_centralWidget);
  setWidget(m_centralWidget);

  m_view = new CdeGraphicsView(p_filename);
  std::cout << "GraphicsView generated" << std::endl;

  m_ui.gridLayout->addWidget(m_view, 3, 0);
  m_ui.m_viewLabel->setText(p_filename + "  "
                            + QString::fromStdString(std::to_string(p_ID)));
  setQtConnections(p_parent);
  installEventFilter(this);
}

void
CdeView::mouseReleaseEvent(QMouseEvent *p_event)
{
  if (p_event->button() == Qt::LeftButton)
    {
      emit gotSelected(this);
    }
  if (p_event->button() == Qt::RightButton)
    {
    }
}

bool
CdeView::eventFilter(QObject *object, QEvent *event)
{
  if (event->type() == QEvent::MouseButtonRelease && (m_view != object))
    {
    }
  return false;
}

void
CdeView::setQtConnections(QWidget *p_parent)
{
  QObject::connect(this, SIGNAL(gotSelected(CdeView *)), p_parent,
                   SLOT(handleSelectedViewChanged(CdeView *)));
  QObject::connect(p_parent, SIGNAL(viewAdded(int, int)), this,
                   SLOT(handle_addConnectionTickBox(int, int)));
  QObject::connect(p_parent, SIGNAL(viewRemoved(int)), this,
                   SLOT(removeConnectionTickBox(int)));
  QObject::connect(this, SIGNAL(viewClosed()), p_parent,
                   SLOT(handle_viewClosed()));
  QObject::connect(this, SIGNAL(connectionAdded(int, int, bool)), p_parent,
                   SLOT(handle_connectionAdded(int, int, bool)));
  QObject::connect(this, SIGNAL(connectionRemoved(int, int, bool)), p_parent,
                   SLOT(handle_connectionRemoved(int, int, bool)));
  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  QObject::connect(m_ui.m_horizontalSlider, SIGNAL(valueChanged(int)), this,
                   SLOT(handle_viewHorizontalChange(int)));
  QObject::connect(m_ui.m_verticalSlider, SIGNAL(valueChanged(int)), this,
                   SLOT(handle_viewVerticalChange(int)));
  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
}

void
CdeView::handleDistantConnectedUpdate()
{
  std::cout << "handeling: handleDistantConnectedUpdate" << std::endl;
}

void
CdeView::handle_addConnectionTickBox(int viewID, int activeViewsCnt)
{
  std::cout << "handle_addConnectionTickBox: " << __LINE__ << std::endl;
  addConnectionTickBox(viewID, activeViewsCnt);
}

void
CdeView::addConnectionTickBox(int viewID, int activeViewsCnt)
{
  if (m_checkBoxes.find(viewID) == m_checkBoxes.end())
    {
      QString newName = QString::number(viewID) + ":";
      m_checkBoxes[viewID] = new CdeCheckBox(newName, viewID);
      m_ui.m_checkBoxes->addWidget(m_checkBoxes[viewID]);
      connect(m_checkBoxes[viewID], SIGNAL(stateChanged(int, int, bool)), this,
              SLOT(handle_connectionChange(int, int, bool)));
    }
}

void
CdeView::removeConnectionTickBox(int viewID)
{
  if (m_checkBoxes.find(viewID) != m_checkBoxes.end())
    {
      m_ui.m_checkBoxes->removeWidget(m_checkBoxes[viewID]);
      m_checkBoxes.erase(viewID);
    }
}

void
CdeView::handle_connectionChange(int state, int p_destID, bool both)
{
  Debuging(Debug::VIEW, "handeling connection change with state: ", state,
           "and destination: ", p_destID);
  if (state < 0)
    {
      emit connectionRemoved(m_ID, p_destID, both);
    }
  else
    {
      emit connectionAdded(m_ID, p_destID, both);
    }
}

void
CdeView::closeEvent(QCloseEvent *e)
{
  emit viewClosed();
}

void
CdeView::handle_viewHorizontalChange(int p_hChange)
{
  std::cout << "handle_viewHChange()" << std::endl;
  emit viewChanged(p_hChange, m_ui.m_verticalSlider->value());
}
void
CdeView::handle_viewVerticalChange(int p_vChange)
{
  Debuging(Debug::VIEW, "called");
  emit viewChanged(m_ui.m_horizontalSlider->value(), p_vChange);
}

void
CdeView::handle_viewUpdate(int p_hChange, int p_vChange)
{
  Debuging(Debug::VIEW, "called");
  m_ui.m_verticalSlider->setValue(p_vChange);
  m_ui.m_horizontalSlider->setValue(p_hChange);
}

void
CdeView::setCheckBox(int boxID, bool state)
{
  std::cout << boxID << " from " << m_ID << std::endl;
  Debuging(Debug::VIEW, "setting checkBox ", boxID, "  in ", m_ID, " to ",
           state);
  if (m_checkBoxes.find(boxID) == m_checkBoxes.end())
    {
      std::cout << "Error: could not set connection from view with ID: "
                << boxID << " to " << m_ID << std::endl;
    }
  else
    {
      m_checkBoxes[boxID]->setChecked(state);
    }
}

bool
CdeView::isConnectedTo(int p_dest)
{
  return m_checkBoxes[p_dest]->isChecked();
}

void
CdeView::saveField()
{
  std::cout << "m_fileName " << m_fileName << std::endl;
  m_view->writeField(m_fileName.c_str());
}
void
CdeView::saveField(std::string p_fileName)
{
  std::cout << "m_fileName " << p_fileName << std::endl;
  m_view->writeField(p_fileName.c_str());
}

void
CdeView::setSelected(bool p_selected)
{
  if (p_selected)
    {
      m_view->setStyleSheet("border: 1px solid red");
    }
  else
    {
      m_view->setStyleSheet("");
    }
}

void
CdeView::updateColors(double p_min, double p_max)
{
  // m_view->updateColors(p_min, p_max);
}

void
changeDisplayedVariable(int p_varID)
{
}
