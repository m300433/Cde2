#include <QGraphicsPolygonItem>
#include <QPointF>

#include <QImage>
#include <QPainter>
#include <QApplication>
#include <QToolTip>
#include <QProgressDialog>

// Cde
#include "cdeImageGenerator.h"
#include "cdeGraphicsView.h"
#include "field.h"
#include "debugOutput.h"

// Colors
#include "../colors/colorScheme.h"
#include "../colors/colorDefinitions.h"
#include "../colors/colorDistributionFunctions.h"
#include "cdePolygonMath.h"
#include "cdeEditContextMenu.h"

#include <thread>
#include <future>
#include <chrono>

constexpr int numThreads = 4;

CdeGraphicsView::CdeGraphicsView(const QString p_fileName, QWidget *p_parent)
    : QGraphicsView(p_parent)
{
  int success = initScene(p_fileName);
  if (success == 1)
    {
      m_manager->addSubscenes(m_scene);
    }
  else
    {
      std::cout << "If this is a fault: here we should destroy the window or "
                   "something"
                << std::endl;
    }
}

int
CdeGraphicsView::initScene(const QString p_fileName)
{
  m_scene = new QGraphicsScene(this);
  setScene(m_scene);
  if (-1 != m_field.open(p_fileName))
    {
      m_colorScheme
          = ColorScheme(create_splitted_linear_distribution(
                            orography.size(), m_field.m_fieldStats.min,
                            m_field.m_fieldStats.max),
                        orography);
      // first time where colorSch and field are initialized
      m_manager
          = std::make_unique<CdeSceneManager>(m_field, m_colorScheme, m_scale);
      return 1;
    }
  else
    {
      std::cout << "could not init CdeGraphicsView for : "
                << p_fileName.toStdString() << std::endl;
    }
  return -1;
}

void
CdeGraphicsView::buildCellLabel(int p_ID, QPolygonF &poly)
{
  std::cout << "P_ID: " << p_ID << std::endl;
  CellLabel cellLabel;
  QPointF offset = QPointF(m_scale, -m_scale);
  double ellipseRadius = m_scale / 2;
  double ellipseCenterOffset = ellipseRadius / 2;
  const QPointF center = getPolygonCenter(poly);
  const QPointF labelPos = center + offset;

  cellLabel.m_text
      = new QGraphicsTextItem(QString::number(m_field.m_field[p_ID]));

  cellLabel.m_rect = m_scene->addRect(cellLabel.m_text->boundingRect(),
                                      QPen(Qt::black), QBrush(Qt::white));
  m_scene->addItem(cellLabel.m_text);

  auto boundingRect = cellLabel.m_rect->boundingRect();

  QPointF splitPoint
      = center
        + QPointF((labelPos.x() - center.x()) / 2.0,
                  (labelPos.y() + (boundingRect.height() / 2.0)) - center.y());

  cellLabel.m_firstLine = m_scene->addLine(QLineF(center, splitPoint)),
  QPen(Qt::black, 10);
  cellLabel.m_secondLine = m_scene->addLine(
      QLineF(splitPoint, labelPos + QPointF(0, (boundingRect.height() / 2)))),
  QPen(Qt::black, 10);
  // auto *ellipse = m_scene->addEllipse(center.x(), center.y(), ellipseRadius,
  // ellipseRadius,  QPen(Qt::black, 0), QBrush(Qt::red));

  cellLabel.m_text->setPos(labelPos);
  cellLabel.m_rect->setPos(labelPos);
  cellLabel.m_text->setFlag(QGraphicsItem::ItemIgnoresTransformations, true);
  cellLabel.m_rect->setFlag(QGraphicsItem::ItemIgnoresTransformations, true);

  m_displayedLabels[p_ID] = std::move(cellLabel);
}

void
CdeGraphicsView::keyPressEvent(QKeyEvent *p_event)
{
  if (p_event->key() == Qt::Key_Q)
    {
      std::cout << "Q was pressed" << std::endl;
      const QPointF pos = mapToScene(mapFromGlobal(QCursor::pos()));
      QGraphicsRectItem *centerPos = qgraphicsitem_cast<QGraphicsRectItem *>(
          m_scene->itemAt(pos, QTransform()));
      std::cout << m_scene->itemAt(pos, QTransform())->type() << std::endl;
      if (centerPos)
        std::cout << centerPos->x() << " " << centerPos->y() << std::endl;
      else
        {
          std::cout << " some bs happend" << std::endl;
          return;
        }
      std::cout << pos.x() << " " << pos.y() << std::endl;
      auto subScene = m_manager->mapPointToSubScene(pos);
      unsigned int sceneIDx = subScene->getIdxFromPos(pos);
      unsigned int ID = -1;
      QPolygonF poly;
      std::cout << "WTF: " << ID << " " << sceneIDx << std::endl;
      if (sceneIDx >= 0)
        {
          ID = subScene->getGlobalIdxFromPos(pos);
          std::cout << ID << std::endl;
        }
      if (ID >= 0)
        {
          if (m_displayedLabels.find(ID) == m_displayedLabels.end())
            {
              std::cout << "CL building" << std::endl;
              buildCellLabel(ID, poly);
            }
          else
            {
              CellLabel &cellLabel = m_displayedLabels[ID];
              m_scene->removeItem(cellLabel.m_rect);
              m_scene->removeItem(cellLabel.m_firstLine);
              m_scene->removeItem(cellLabel.m_secondLine);
              m_scene->removeItem(cellLabel.m_text);
              m_displayedLabels.erase(ID);
            }
        }
      else
        {
          std::cout << "No cell added since ID could not be found" << std::endl;
        }
    }
}

void
CdeGraphicsView::mousePressEvent(QMouseEvent *p_event)
{
  m_rubberBandOrigin = p_event->pos();
  m_mousePressed = true;
}

void
CdeGraphicsView::mouseMoveEvent(QMouseEvent *p_event)
{
  if (m_mousePressed && m_rubberBand == nullptr)
    {
      if ((m_rubberBandOrigin - p_event->pos()).manhattanLength() > m_scale)
        {
          m_rubberBand = new QRubberBand(QRubberBand::Rectangle, this);
          m_rubberBand->setGeometry(QRect(m_rubberBandOrigin, QSize()));
          m_rubberBand->show();
        }
    }
  if (m_rubberBand != nullptr)
    {
      m_rubberBand->setGeometry(
          QRect(m_rubberBandOrigin, p_event->pos()).normalized());
    }
}
void
CdeGraphicsView::mouseReleaseEvent(QMouseEvent *p_event)
{
  m_mousePressed = false;
  if (m_rubberBand != nullptr)
    {
      QPoint rubberBandEnd = p_event->pos();

      QRectF zoomRectInScene
          = QRectF(mapToScene(m_rubberBandOrigin), mapToScene(rubberBandEnd));

      fitInView(zoomRectInScene, Qt::KeepAspectRatio);
      m_rubberBand->hide();
      delete m_rubberBand;
      m_rubberBand = nullptr;
    }

  else if (p_event->button() == Qt::LeftButton)
    {

      const QPoint eventPos = p_event->pos();
      QPointF pos = mapToScene(eventPos);
      auto subScene = m_manager->mapPointToSubScene(pos);
      unsigned int globalIdx = subScene->getGlobalIdxFromPos(pos);

      if (globalIdx >= 0)
        {
          handleSelectionChange(globalIdx);
        }
      return;
    }
  if (p_event->button() == Qt::RightButton)
    {
      const QPoint eventPos = p_event->pos();
      QPointF pos = mapToScene(eventPos);
      auto subScene = m_manager->mapPointToSubScene(pos);
      unsigned int entryId = subScene->getGlobalIdxFromPos(pos);
      if (entryId < 0)
        {
          return;
        }

      if (m_selections.size() > 0
          && m_selections.find(entryId) != m_selections.end())
        {
          CdeEditContextMenu *menu = new CdeEditContextMenu(this);

          QAction *m_actionEditAll = new QAction("Edit selected", this);
          QAction *m_actionEditSingle = new QAction("Edit", this);
          menu->addAction(m_actionEditAll);
          menu->addAction(m_actionEditSingle);

          QAction *result = menu->exec(p_event->globalPos());
          if (result)
            {
              menu->openEditWindow();
              std::cout << menu->getResult() << std::endl;
              handleEdit(menu->getResult());
            }
          delete menu;
        }
      else
        {
          m_selectedEntriesIdx.clear();
          for (auto entry : m_selectionPolys)
            {
              m_scene->removeItem(entry);
            }
          m_selectionPolys.clear();
        }
    }
}
bool
CdeGraphicsView::eventFilter(QObject *object, QEvent *event)
{
  return true;
}

void
CdeGraphicsView::wheelEvent(QWheelEvent *e)
{
  setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
  if (e->angleDelta().x() == 0)
    {

      QPoint pos = e->pos();
      QPointF posf = this->mapToScene(pos);

      double by;
      double angle = 2 * e->angleDelta().y();

      if (angle > 0)
        {
          by = 1 + (angle / 360 * 0.1);
        }
      else if (angle < 0)
        {
          by = 1 - (-angle / 360 * 0.1);
        }
      else
        {
          by = 1;
        }

      this->scale(by, by);

      double w = this->viewport()->width();
      double h = this->viewport()->height();

      double wf = this->mapToScene(QPoint(w - 1, 0)).x()
                  - this->mapToScene(QPoint(0, 0)).x();
      double hf = this->mapToScene(QPoint(0, h - 1)).y()
                  - this->mapToScene(QPoint(0, 0)).y();

      double lf = posf.x() - pos.x() * wf / w;
      double tf = posf.y() - pos.y() * hf / h;

      /* try to set viewport properly */
      this->ensureVisible(lf, tf, wf, hf, 0, 0);

      QPointF newPos = this->mapToScene(pos);

      /* readjust according to the still remaining offset/drift
       * I don't know how to do this any other way */
      // this->ensureVisible(
      //    QRectF(QPointF(lf, tf) - newPos + posf, QSizeF(wf, hf)), 0, 0);

      e->accept();
    }
}

void
CdeGraphicsView::handleSelectionChange(int p_fieldEntryIdx)
{
  if (QApplication::keyboardModifiers() != Qt::ShiftModifier)
    {
      for (auto &sel : m_selections)
        {
          m_scene->removeItem(sel.second);
        }
      m_selections.clear();
    }

  if (p_fieldEntryIdx >= 0)
    {
      if (m_selections.find(p_fieldEntryIdx) == m_selections.end())
        {
          QPolygonF newPoly = m_field.generatePoly(0, p_fieldEntryIdx, m_scale);
          m_selections[p_fieldEntryIdx] = m_scene->addPolygon(
              newPoly, QPen(Qt::green, 0), QBrush(Qt::NoBrush));
          m_selections[p_fieldEntryIdx]->setZValue(100);
        }
      else
        {
          std::cout
              << "removing of already selected is not implemented right now"
              << std::endl;
        }
      /* clang-format off */
    }
  else { std::cout << "invalid id" << std::endl; }
  /* clang-format on */
}
/*
void CdeGraphicsView::nameLater()
{

}
*/
void
CdeGraphicsView::drawSelectionPolygon(int p_fieldEntryIdx)
{
  std::cout << "WARNING: use of old and now unimplemented function: "
               "drawSelectionPolygon"
            << std::endl;
}

void
CdeGraphicsView::handleEdit(double p_newValue)
{

  Debuging(Debug::VIEW, "handleEditAll");

  std::set<std::shared_ptr<CdeSubScene>> changedSubScenes;
  for (auto &sel : m_selections)
    {
      m_field.m_field[sel.first] = p_newValue;
      auto poly = sel.second->polygon();
      QPointF centerPos = getPolygonCenter(poly);
      auto subScene = m_manager->mapPointToSubScene(centerPos);

      QPainter *painter = new QPainter(&subScene->m_pixmap);
      subScene->updatePixmapPoly(sel.first, painter);
      changedSubScenes.insert(subScene);
      delete painter;
    }
  for (auto changedScene : changedSubScenes)
    {
      std::cout << "Updated: " <<   changedScene->m_id << std::endl;
      changedScene->updatePixmapItem();
    }
  Debuging(Debug::VIEW, "handleEditAll done");
}

void
CdeGraphicsView::writeField(std::string p_fileName)
{
  m_field.printField();
  m_field.writeField(p_fileName.c_str());
}

void
CdeGraphicsView::updateColors(double p_min, double p_max)
{
  std::cout << "updateColor called" << std::endl;
  m_colorScheme = ColorScheme(
      create_splitted_linear_distribution(orography.size(), p_min, p_max),
      orography);
}
