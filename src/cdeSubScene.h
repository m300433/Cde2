#ifndef CDESUBSCENE_H
#define CDESUBSCENE_H

// Qt
#include <QGraphicsTextItem>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QPen>
#include <QRect>
#include <QPixmap>
// color
#include "../colors/colorScheme.h"

#include "field.h"
#include "cdeFieldMaxima.h"
#include "cdeOptions.h"
#include <mutex>

class CdeSubScene
{
public:
  QRectF m_sectionRect;
  int m_id;
  int r, g, b;
  Field &m_field;
  ColorScheme &m_colorScheme;

  QPixmap m_pixmap;
  QGraphicsPixmapItem *m_pixmapItem;
  double &m_scale;
  std::vector<unsigned int> m_entryIDs;
  QPen m_pen = QPen(Qt::NoPen);
  std::mutex addElementMutex;

  CdeSubScene(Field &p_field, ColorScheme &p_colorScheme, double &p_scale);
  CdeSubScene(CdeSubScene&) = delete;
  void initPixmap();
  void updateSize(QPolygonF &p_poly);
  void add(int p_fieldEntryIdx, QPolygonF &p_poly);
  QGraphicsPixmapItem *addToScene(QGraphicsScene *p_scene);
  unsigned int getGlobalIdxFromPos(const QPointF &p_pos);
  unsigned int getIdxFromPos(const QPointF &p_pos);
  void updateColors(ColorScheme &p_colorScheme);
  void createPixmap();
  void handleDebug(QGraphicsScene* p_scene);
  void updatePixmapPoly(unsigned int idx, QPainter *painter);
  void updatePixmapItem();
};
#endif
