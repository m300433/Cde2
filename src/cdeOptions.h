#ifndef CDEOPTIONS_H
#define CDEOPTIONS_H

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <vector>
#include <iostream>
#include <iomanip>

static struct option long_options[]
    = { /* These options set a flag. */
        { "debug", no_argument, 0, 'd' },
        /* These options don’t set a flag.
           We distinguish them by their indices. */
        { "debugLevel", required_argument, 0, 'D' },
        { 0, 0, 0, 0 }
      };

namespace CdeOptions
{
  extern  bool debug;
  extern int debugLevel;
  extern std::vector<std::string> filenames;

  void parse(int argc, char **argv);
  void print();
};  // namespace CdeOptions

#endif
