#include "cdeOptions.h"
namespace CdeOptions
{

  bool debug = false;
  int debugLevel = 0;
  std::vector<std::string> filenames;
  void
  print()
  {
    std::cout << "Debug     :" << std::setw(20) << debug << std::endl;
    std::cout << "DebugLevel:" << std::setw(20) << debugLevel << std::endl;
    std::cout << "File names:" << std::setw(20);
    for (auto fname : filenames)
      std::cout << fname << " ";
    std::cout << std::endl;
  }
  void
  parse(int argc, char **argv)
  {
    int c = 0;

    int option_index = 0;
    while (c != -1)
      {
        c = getopt_long(argc, argv, "dD:", long_options, &option_index);

        /* Detect the end of the options. */
        switch (c)
          {
          case 0:
            /* If this option set a flag, do nothing else now. */
            break;

          case 'D':
            debug = true;
            debugLevel = std::stoi(optarg);
            break;

          case 'd': debug = true; break;
          case '?':
            /* getopt_long already printed an error message. */
            break;
          }
      }

    if (optind < argc)
      {
        while (optind < argc)
          {
            filenames.push_back(argv[optind]);
            optind++;
          }
      }
  }
}  // namespace CdeOptions
