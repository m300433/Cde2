#include "ui/ui_MainWindow.h"

#include <vector>

class CdeView;

class CdeMainWindow : public QMainWindow
{
  Q_OBJECT
public:
  CdeMainWindow(QMainWindow *parent = nullptr);

private:
  void connect();
  Ui::MainWindow m_ui;
  std::vector<CdeView *> m_selectedViews;
  std::map<int, CdeView *> m_views;
  void mouseReleaseEvent(QMouseEvent *p_event);
  bool eventFilter(QObject *object, QEvent *event);
private slots:
  void handle_viewClosed();
  void handleOpenFileRequest();
  void handleSaveFileRequest();
  void handleSaveAsFileRequest();
  void handleSelectedViewChanged(CdeView *p_newlySelected);

  void handle_connectionAdded(int source, int dest, bool);
  void handle_connectionRemoved(int source, int dest, bool);

  void handle_colorRangeUpdate();
signals:
  void viewAdded(int viewCnt, int activeViewCnt);
  void viewRemoved(int);
};
