#include "cdeMainWindow.h"
#include "cdeView.h"
#include "debugOutput.h"
#include <QMouseEvent>
#include <QDoubleValidator>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QGraphicsView>
#include <iostream>
#include <QFileDialog>

static int view_count = 0;
static int activeViewsCnt = 0;

CdeMainWindow::CdeMainWindow(QMainWindow *parent) : QMainWindow(parent)
{
  m_ui.setupUi(this);
  m_ui.centralwidget->installEventFilter(this);
  m_ui.m_lineEditMin->setValidator(
      new QDoubleValidator(std::numeric_limits<short>::min(),
                           std::numeric_limits<short>::max(), 3, this));
  m_ui.m_lineEditMin->setValidator(
      new QDoubleValidator(std::numeric_limits<short>::min(),
                           std::numeric_limits<short>::max(), 3, this));
  connect();
}

void
CdeMainWindow::handle_viewClosed()
{
  std::cout << "something was closed" << std::endl;
  Debuging(Debug::MAIN, "Something was closed");
}

void
CdeMainWindow::handleSaveFileRequest()
{
  unsigned int selectedViewsCnt = m_selectedViews.size();
  if (selectedViewsCnt > 1)
    {
      std::cout << "handling multiple selected" << std::endl;
    }
  else if (selectedViewsCnt == 1)
    {
      m_selectedViews.back()->saveField();
    }
  else
    {
      std::cout << "handling 0 selected" << std::endl;
    }
}
void
CdeMainWindow::handleSaveAsFileRequest()
{
  unsigned int selectedViewsCnt = m_selectedViews.size();
  QFileDialog *fileDiag
      = new QFileDialog(this, "Save File", "/home/oliver/Code/MaxP/cde_2/data");
  fileDiag->setAcceptMode(QFileDialog::AcceptSave);
  QStringList fileNameList;
  fileDiag->exec();
  fileDiag->setWindowModality(Qt::WindowModal);
  fileNameList = fileDiag->selectedFiles();
  for (QString a : fileNameList)
    {
      if (selectedViewsCnt > 1)
        {
          std::cout << "handling multiple selected" << std::endl;
        }
      else if (selectedViewsCnt == 1)
        {
          m_selectedViews.back()->saveField(fileNameList[0].toStdString());
        }
      else
        {
          std::cout << "handling 0 selected" << std::endl;
        }

      std::cout << a.toStdString() << std::endl;
    }
  std::cout << "handling save as" << std::endl;
}
#include <unistd.h>
void
CdeMainWindow::handleOpenFileRequest()
{
  QFileDialog *fileDiag
      = new QFileDialog(this, "Open File", "/home/oliver/Code/MaxP/cde_2/data");
  fileDiag->setFileMode(QFileDialog::ExistingFiles);
  QStringList fileNameList;
  fileDiag->exec();
  fileDiag->setWindowModality(Qt::WindowModal);
  fileNameList = fileDiag->selectedFiles();
  fileDiag->hide();
  for (const QString fileName : fileNameList)
    {
      CdeView *newView = new CdeView(fileName, view_count, this);
      for (auto view : m_views)
        {
          newView->addConnectionTickBox(view.first, 0);
        }

      m_views[view_count] = newView;
      Debuging(Debug::MAIN_EMIT, "emmiting viewAdded");
      emit viewAdded(view_count, activeViewsCnt);

      m_ui.m_viewLayout->addWidget(m_views[view_count]);
      view_count++;
      activeViewsCnt++;
    }
  delete fileDiag;
}

bool
CdeMainWindow::eventFilter(QObject *object, QEvent *event)
{
  if (event->type() == QEvent::KeyRelease)
    {
    }
  return QMainWindow::eventFilter(object, event);
}

void
CdeMainWindow::connect()
{
  QObject::connect(m_ui.m_actionOpen, SIGNAL(triggered()), this,
                   SLOT(handleOpenFileRequest()));
  QObject::connect(m_ui.m_actionSave, SIGNAL(triggered()), this,
                   SLOT(handleSaveFileRequest()));
  QObject::connect(m_ui.m_actionSaveAs, SIGNAL(triggered()), this,
                   SLOT(handleSaveAsFileRequest()));

  QObject::connect(m_ui.m_lineEditMax, SIGNAL(returnPressed()), this,
                   SLOT(handle_colorRangeUpdate()));
  QObject::connect(m_ui.m_lineEditMin, SIGNAL(returnPressed()), this,
                   SLOT(handle_colorRangeUpdate()));
  QObject::connect(m_ui.m_ButtonConfirmNewColorRange, SIGNAL(released()), this,
                   SLOT(handle_colorRangeUpdate()));
}

void
CdeMainWindow::mouseReleaseEvent(QMouseEvent *p_event)
{
  Debuging(Debug::MOUSE_EVENT, "MainWindow");
  if (p_event->button() == Qt::LeftButton)
    {
      childAt(p_event->pos());
    }
  if (p_event->button() == Qt::RightButton)
    {
    }
}

void
CdeMainWindow::handleSelectedViewChanged(CdeView *p_newlySelected)
{
  std::cout << "something changed " << std::endl;
  for (int i = 0; i < m_selectedViews.size(); i++)
    {
      if (m_selectedViews[i] == p_newlySelected)
        {
          m_selectedViews[i]->setSelected(false);
          m_selectedViews[i]->clearFocus();
          m_selectedViews.erase(m_selectedViews.begin() + i);
          return;
        }
    }
  if (p_newlySelected)
    {
      p_newlySelected->setSelected(true);
      if (QApplication::keyboardModifiers() == Qt::ShiftModifier)
        {
          m_selectedViews.push_back(p_newlySelected);
        }
      else
        {
          for (auto view : m_selectedViews)
            {
              view->setSelected(false);
            }
          m_selectedViews.clear();
          m_selectedViews.push_back(p_newlySelected);
        }
    }
  else
    {
      std::cout << "fired without new selection" << std::endl;
    }
}
void
CdeMainWindow::handle_connectionRemoved(int p_source, int p_dest, bool both)
{

  auto source = m_views[p_source];
  auto dest = m_views[p_dest];
  DebugingDisconnect(Debug::MAIN_CONNECT, "view", p_source, "view", p_dest,
                     "viewChanged", "viewUpdate");
  QObject::disconnect(source, SIGNAL(viewChanged(int, int)), dest,
                      SLOT(handle_viewUpdate(int, int)));
  if (both && dest->isConnectedTo(p_source))
    {
      // also disconnects in reverse direction
      dest->setCheckBox(p_source, false);
    }
}

void
CdeMainWindow::handle_connectionAdded(int p_source, int p_dest, bool both)
{
  auto source = m_views[p_source];
  auto dest = m_views[p_dest];
  DebugingConnect(Debug::MAIN_CONNECT, "view", p_source, "view", p_dest,
                  "viewChanged", "viewUpdate");
  QObject::connect(source, SIGNAL(viewChanged(int, int)), dest,
                   SLOT(handle_viewUpdate(int, int)));
  if (both && !dest->isConnectedTo(p_source))
    {
      // also connects in reverse direction
      dest->setCheckBox(p_source, true);
    }
}

void
CdeMainWindow::handle_colorRangeUpdate()
{
  if (m_views.size() == 1)
    {
      double test = m_ui.m_lineEditMax->text().toDouble();
      double test2 = m_ui.m_lineEditMin->text().toDouble();
      m_views.begin()->second->updateColors(test2, test);
      std::cout << test << " " << test2 << std::endl;
    }
}
