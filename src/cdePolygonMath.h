#ifndef CDEPOLYGONMATH_H
#define CDEPOLYGONMATH_H

#include <QPolygonF>
#include <QPointF>

bool polygonContainsPoint(QPolygonF p_poly, QPointF p_point);

QPointF getPolygonCenter(QPolygonF &p_polygon);

#endif
