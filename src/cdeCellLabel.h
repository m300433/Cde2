#ifndef CDECELLLABLE_H
#define CDECELLLABLE_H

struct CellLabel
{
  QGraphicsScene *m_sceneRef;
  QGraphicsRectItem *m_rect;
  QGraphicsLineItem *m_firstLine;
  QGraphicsLineItem *m_secondLine;
  QGraphicsTextItem *m_text;
};

#endif
