#ifndef CDESCENEMANAGER_H
#define CDESCENEMANAGER_H

// Std
#include <vector>
// Qt
#include <QRect>
#include <QPolygonF>
#include <QPixmap>
#include <QGraphicsScene>
// Cde
#include "field.h"
#include "cdeFieldMaxima.h"
#include "cdeSubScene.h"
// Color
#include "../colors/colorScheme.h"

// Temp : if there are includes here: DELETE THEM
#include <QGraphicsTextItem>

class CdeSceneManager
{
public:

  Field &m_field;
  void init(Field &p_field, ColorScheme &p_colorScheme);
  int m_cntVerticalSections;
  int m_cntHorizontalSections;
  std::vector<std::vector<std::shared_ptr<CdeSubScene>>> m_subScenes;
  double &m_scale;



  CdeSceneManager(Field &p_field, ColorScheme &p_colorScheme, double &p_scale);
  void initSubSections(Field &p_field, ColorScheme &p_colorScheme);
  void generateSubScenes(Field &p_field, unsigned int p_cntSubScenes,
                         ColorScheme &p_colorScheme);
  std::shared_ptr<CdeSubScene> mapPointToSubScene(const QPointF &p_point);
  std::shared_ptr<CdeSubScene> mapPolyToSubScene(QPolygonF &p_poly);

  void generatePolygon();
  void addPolygon(int p_fieldEntryIdx, QPolygonF p_poly);
  void addSubscenes(QGraphicsScene *p_scene);
  void updateColors(ColorScheme &p_colorScheme);
  void addPolys(Field &p_field, unsigned int start, unsigned int end);
  void calculateSubSceneCnt(unsigned int cntPolys, unsigned int wanted);
};

#endif
