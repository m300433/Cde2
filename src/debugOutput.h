/*
  This file is part of CDO. CDO is a collection of Operators to
  manipulate and analyse Climate model Data.

  Copyright (C) 2003-2018 Uwe Schulzweida, <uwe.schulzweida AT mpimet.mpg.de>
  See COPYING file for copying and redistribution conditions.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
*/
#ifndef DEBUG_SWITCHES_H
#define DEBUG_SWITCHES_H
#include <iostream>
#include <sstream>

#include <fstream>
#include <string.h>

namespace Log
{
  void StdOut(std::stringstream &message);

  template <typename... T>
  static void
  expand(std::stringstream &p_message, T &&... args)
  {
    // for showing that the dummy array is never used
    using expander = int[];
    // creating dummy array with expanding the parameter pack in its
    // initializer list while writing each element of the pack into message
    expander{ 0, (void(p_message << std::forward<T>(args)), 0)... };
    p_message << std::endl;
  }

  template <typename... T>
  void
  StdOut(T &&... args)
  {
    std::stringstream message;
    expand(message, args...);
    std::cout << message.str();
  }

  template <typename... T>
  void
  StdErr(T &&... args)
  {
    std::stringstream message;
    expand(message, args...);
    std::cout << message.str();
  }
}  // namespace Log

namespace Debug
{

  extern int VIEW;
  extern int VIEW_EMIT;
  extern int VIEW_CONNECT;
  extern int MAIN;
  extern int MAIN_EMIT;
  extern int MAIN_CONNECT;
  extern int MOUSE_EVENT;
  extern int CHECK_BOX;
  extern int THREADING;
  extern int IMAGE_CREATION;
  extern int SUBSCENE;
  extern int SCENEMANAGER;
  // File switches and streams
  extern std::string outfile;
  extern bool print_to_seperate_file;
  extern std::fstream outfile_stream;

  std::string get_padding(const char *p_func);

  void StartMessage();
  void EndMessage();
  void SetDebug(int p_debug_level);
  std::string argvToString(int argc, const char **argv);

  void printMessage(std::stringstream &p_message, bool both = false);
  void printError(std::stringstream &p_message, bool both = false);
  void DebugConnect(const char *p_func, int scope, std::string type1,
                    int source, std::string type2, int dest,
                    std::string signalName, std::string slotName);
  void DebugDisconnect(const char *p_func, int scope, std::string type1,
                       int source, std::string type2, int dest,
                       std::string signalName, std::string slotName);

  template <typename... T>
  void
  Message_(const char *p_func, T &&... args)
  {
    std::stringstream message;
    message << p_func << ": " << get_padding(p_func);
    Log::expand(message, args...);
    printMessage(message);
  }

  template <typename... T>
  void
  PrintDebug(const char *p_func, int p_debugScope, T &&... args)
  {
    if (p_debugScope > 0)
      {
        std::stringstream message;
        message << p_func << ": " << get_padding(p_func);
        Log::expand(message, args...);
        printMessage(message);
      }
  }

  template <typename... T>
  void
  Warning_(T &&... args)
  {
    std::stringstream message;
    message << "Warning: ";
    Log::expand(message, args...);
    printMessage(message, true);
  }

}  // namespace Debug

namespace Error
{
  static int _ExitOnError = 1;

  template <typename... T>
  void
  Abort(T &&... args)
  {
    std::stringstream message;
    message << " (Abort): ";
    Log::expand(message, args...);
    Debug::printError(message, true);
    if (Error::_ExitOnError)
      {
        if (Debug::print_to_seperate_file) Debug::outfile_stream.close();
        exit(EXIT_FAILURE);
      }
  }

  template <typename... T>
  void
  Error_(const char *p_file, const int p_line, const char *caller, T &&... args)
  {
    std::stringstream message;
    message << "Error in: " << p_file << ":" << p_line << " " << caller << " ";
    Log::expand(message, args...);
    Debug::printError(message, true);
    if (Error::_ExitOnError)
      {
        if (Debug::print_to_seperate_file) Debug::outfile_stream.close();
        exit(EXIT_FAILURE);
      }
  }

  template <typename... T>
  void
  SysError_(const char *p_file, const int p_line, const char *p_func,
            T &&... args)
  {
    int saved_errno = errno;
    std::stringstream message;
    message << "SysError in:" << p_file << std::endl;
    message << "    "
            << "in function: p_func ,line: " << p_line << std::endl;
    Log::StdOut(message, args...);
    if (saved_errno)
      {
        errno = saved_errno;
        perror("Sytem error message");
      }
    exit(EXIT_FAILURE);
  }
}  // namespace Error
#if defined WITH_CALLER_NAME
#define SYS_ERROR(...) \
  Error::SysError_(__FILE__, __LINE__, __func__, __VA_ARGS__)
#define ERROR_C(...) Error::Error_(__FILE__, __LINE__, caller, __VA_ARGS__)
#define ERROR(...) Error::Error_(__FILE__, __LINE__, __func__, __VA_ARGS__)
#define WARNING(...) Error::Warning_(__func__, __VA_ARGS__)
#define MESSAGE_C(...) Debug::Message_(caller, __VA_ARGS__)
#define MESSAGE(...) Debug::Message_(__func__, __VA_ARGS__)
#else
#define SYS_ERROR(...) Error::SysError_(__FILE__, __LINE__, "", __VA_ARGS__)
#define ERROR_C(...) Error::Error_(__FILE__, __LINE__, "", __VA_ARGS__)
#define ERROR(...) Error::Error_(__FILE__, __LINE__, "", __VA_ARGS__)
#define WARNING(...) Error::Warning_(__VA_ARGS__)
#define MESSAGE_C(...) Debug::Message_(__VA_ARGS__)
#define MESSAGE(...) Debug::Message_(__func__, __VA_ARGS__)
#endif
#define Debuging(...) Debug::PrintDebug(__func__, __VA_ARGS__)
#define DebugingConnect(...) Debug::DebugConnect(__func__, __VA_ARGS__)
#define DebugingDisconnect(...) Debug::DebugDisconnect(__func__, __VA_ARGS__)
#endif
