#include "cdeCheckBox.h"

#include <QApplication>
#include <iostream>
#include "debugOutput.h"

CdeCheckBox::CdeCheckBox(QString p_name, int p_ID)
    : QCheckBox(p_name), m_ID(p_ID)
{
  connect(this, SIGNAL(stateChanged(int)), this,
          SLOT(handle_stateChanged(int)));
}

void
CdeCheckBox::handle_stateChanged(int p_state)
{
  Debuging(Debug::CHECK_BOX, "called");
  bool both = false;
  int state = p_state;
  if (state == 0)
    {
      state = -1;
    }
  else
    {
      state = 1;
    }
  if (QApplication::keyboardModifiers() == Qt::ShiftModifier)
    {
      both = true;
    }
  Debuging(Debug::CHECK_BOX, "emmiting state changed to", state, " in box",  m_ID,  " both = ", both);
  emit stateChanged(state, m_ID, both);
}
