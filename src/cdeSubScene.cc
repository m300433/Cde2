#include "cdeSubScene.h"
#include "cdePolygonMath.h"
#include "debugOutput.h"

// QT includes
#include <QPainter>
#include <QPixmap>

CdeSubScene::CdeSubScene(Field &p_field, ColorScheme &p_colorScheme,
                         double &p_scale)
    : m_field(p_field), m_colorScheme(p_colorScheme), m_scale(p_scale)
{
}
#include <thread>
void
CdeSubScene::initPixmap()
{
  Debuging(Debug::SUBSCENE, "thread ", std::this_thread::get_id(), "started");

  r = (std::sin(m_id * (255.f / m_id + 1)) + 1.f) * (255.f / 2.f);
  b = (std::cos(m_id * (255.f / m_id + 1)) + 1.f) * (255.f / 2.f);
  g = r;
  m_pen.setCosmetic(true);
  m_pixmap = QPixmap(m_sectionRect.width(), m_sectionRect.height());
  m_pixmap.fill(Qt::transparent);
  createPixmap();
}

void
CdeSubScene::updatePixmapPoly(unsigned int p_entryID, QPainter *p_painter)
{
  p_painter->setPen(m_pen);
  p_painter->setBrush(
      m_colorScheme.getColorFromValue(m_field.m_field[p_entryID]));
  QPolygonF local = m_field.generatePoly(0, p_entryID, m_scale);
  local.translate(-m_sectionRect.topLeft());
  p_painter->drawPolygon(local);
}
void
CdeSubScene::updatePixmapItem()
{
  m_pixmapItem->setPixmap(m_pixmap);
}
void
CdeSubScene::createPixmap()
{
  QPainter *painter = new QPainter(&m_pixmap);

  for (int i = 0; i < m_entryIDs.size(); i++)
    {
      unsigned int entryID = m_entryIDs[i];
      updatePixmapPoly(entryID, painter);
    }
  painter->end();
  delete painter;
}
void
CdeSubScene::updateSize(QPolygonF &p_poly)
{
  QRectF boundingRect = p_poly.boundingRect();
  for (int i = 0; i < p_poly.size(); i++)
    {
      if (!m_sectionRect.contains(p_poly[i]))
        {
          m_sectionRect = m_sectionRect.united(boundingRect);
        }
    }
}
void
CdeSubScene::add(int p_fieldEntryIdx, QPolygonF &p_poly)
{
  std::lock_guard<std::mutex> guard(addElementMutex);
  updateSize(p_poly);
  m_entryIDs.push_back(p_fieldEntryIdx);
}

void
CdeSubScene::handleDebug(QGraphicsScene* p_scene)
{
    /*
      int l = 0;
      for (auto p : m_polys)
        {
          auto test = p_scene->addText(QString::number(m_id) + " "
                                       + QString::number(m_entryIDs[l]) + " "
                                       + QString::number(l));
          test->setPos(getPolygonCenter(p));
          test->adjustSize();
          test->setZValue(100);
          l++;
        }
      */
  auto sceneRect = p_scene->addRect(m_sectionRect, QPen(QColor(r, g, b), 15));
  sceneRect->setZValue(100);
}

QGraphicsPixmapItem *
CdeSubScene::addToScene(QGraphicsScene *p_scene)
{
  Debuging(Debug::SUBSCENE, "start adding polys ", m_id);
  m_pixmapItem = p_scene->addPixmap(m_pixmap);
  m_pixmapItem->setPos(m_sectionRect.topLeft());
  m_pixmapItem->setZValue(1);
  if(CdeOptions::debug)
  {
        handleDebug(p_scene);
  }

  Debuging(Debug::SUBSCENE, "end adding polys ", m_id);
  return m_pixmapItem;
}
unsigned int
CdeSubScene::getGlobalIdxFromPos(const QPointF &p_pos)
{
  int id = getIdxFromPos(p_pos);
  if (id >= 0)
    {
      std::cout << "ID found: " << id << std::endl;
      return m_entryIDs[id];
    }
  std::cout << "returning -1" << std::endl;
  return -1;
}

unsigned int
CdeSubScene::getIdxFromPos(const QPointF &p_pos)
{
  for (int i = 0; i < m_entryIDs.size(); i++)
    {
      auto poly = m_field.generatePoly(0, m_entryIDs[i], m_scale);
      if (polygonContainsPoint(poly, p_pos))
        {
          return i;
        }
    }
  std::cout << "Warning: could not map point (" << p_pos.x() << " / "
            << p_pos.y() << ") to any field entry" << std::endl;
  std::cout << "returning -1" << std::endl;
  return -1;
}

void
CdeSubScene::updateColors(ColorScheme &p_colorScheme)
{
  m_colorScheme = p_colorScheme;
}
