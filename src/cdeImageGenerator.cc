// C++ std
#include <future>

// QT
#include <QPainter>
#include <QPen>
#include <QLabel>

// cde
#include "cdeImageGenerator.h"
#include "debugOutput.h"

CdeImageGenerator::CdeImageGenerator(QImage &p_image,
                                     std::vector<QPolygonF> &p_polys,
                                     Field &p_field, ColorScheme &p_colorScheme)
    : m_image(p_image), m_polys(p_polys), m_field(p_field),
      m_colorScheme(p_colorScheme)
{
  m_pen = QPen(Qt::black, 0);
  m_pen.setCosmetic(true);
  m_numThreads = 4;
  qRegisterMetaType<std::thread::id>("std::thread::id");
  connect(this, &CdeImageGenerator::threadFinished, this, &CdeImageGenerator::handleThreadFinished);
  initProgressDialog();
  run();
}

void
CdeImageGenerator::initProgressDialog()
{
  m_progressDiag.setRange(0, m_polys.size());
  m_progressDiag.setAutoClose(true);
  m_progressDiag.setLabel(new QLabel("Generating new grid representation"));
  m_progressDiag.setWindowModality(Qt::WindowModal);
  m_progressDiag.setMinimumDuration(0);
  m_progressDiag.setValue(0);
  connect(this, &CdeImageGenerator::progressUpdate, this,
          &CdeImageGenerator::updateProgressBar);
}

void
CdeImageGenerator::updateProgressBar(std::thread::id p_thrID, int p_status)
{
  // Debuging(Debug::IMAGE_CREATION, "updating Progress");
  m_threadStatus[p_thrID] = p_status;
  int sum = 0;
  for (auto threadStatus : m_threadStatus)
    {
      sum += threadStatus.second;
    }
  // Debuging(Debug::IMAGE_CREATION, "new status is: ", sum);
  m_progressDiag.setValue(sum);
}

void
CdeImageGenerator::handleThreadFinished(std::thread::id p_thrID)
{
  Debuging(Debug::IMAGE_CREATION, "thread finished: ", p_thrID);
  m_finishedThreads.insert(p_thrID);
  if (m_finishedThreads.size() == m_numThreads)
    {
      QPainter p(&m_image);
      for (auto image : m_subImages)
        {
          p.drawImage(0, 0, *image.second);
        }
      emit imageDone(m_image);
    }
}

void
CdeImageGenerator::generateImage(int p_startIdx, int p_endIdx)
{
  Debuging(Debug::IMAGE_CREATION,
           "thread started: ", std::this_thread::get_id());
  QImage *image = new QImage(m_image);
  QPainter *painter = new QPainter(image);
  painter->setPen(m_pen);
  painter->setRenderHints(QPainter::HighQualityAntialiasing);

  std::thread::id thrID = std::this_thread::get_id();
  int updateValue = std::max(1, (p_endIdx - p_startIdx) / 100);
  for (int i = p_startIdx; i < p_endIdx; i++)
    {
      if (i % updateValue == 0)
        {
          emit progressUpdate(thrID, i - p_startIdx);
          // progress.setValue(i - p_startIdx);
        }
      painter->setBrush(m_colorScheme.getColorFromValue(m_field.m_field[i]));
      painter->drawConvexPolygon(m_polys[i]);
    }

  Debuging(Debug::IMAGE_CREATION,
           "thread finished: ", std::this_thread::get_id());
  m_subImages[thrID] = image;
  emit threadFinished(thrID);
}

void
CdeImageGenerator::run()
{
  int size = m_polys.size();
  int subSize = size / m_numThreads;
  int rest = size - (subSize * m_numThreads);

  Debuging(Debug::IMAGE_CREATION, " ", size, " ", subSize, " ", rest);

  int i = 0;
  int subVecStart = 0;
  for (; i < m_numThreads - 1; i++)
    {
      subVecStart = subSize * i;
      std::thread t(&CdeImageGenerator::generateImage, this, subVecStart,
                    subVecStart + subSize);
      t.detach();
    }
  subVecStart = subSize * i;
  std::thread t(&CdeImageGenerator::generateImage, this, subVecStart, size);

  t.detach();
}
