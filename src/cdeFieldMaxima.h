#ifndef CDEFIELDMAXIMA_H
#define CDEFIELDMAXIMA_H

#include <iostream>
struct FieldMaxima
{
  double maxH = 0;
  double maxV = 0;
  double minH = 0;
  double minV = 0;
};
/*
static void
printFieldMax(FieldMaxima &maxima)
{
  std::cout << "maxH: " << maxima.maxH << " maxV: " << maxima.maxV << std::endl
            << " minH: " << maxima.minH << " minV: " << maxima.minV
            << std::endl;
}
*/
#endif
