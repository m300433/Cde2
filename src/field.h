#ifndef FIELD_H
#define FIELD_H

#include <math.h>
#include <QDebug>
#include <QPolygonF>
#include <vector>
#include "../cdilib/cdi.h"

#ifndef DBL_IS_EQUAL
/*
#define  DBL_IS_EQUAL(x,y) (fabs(x - y) <= 2.0*(y*DBL_EPSILON + DBL_MIN))
*/
#define DBL_IS_EQUAL(x, y) (!(fabs(x - y) > 0))
#endif

constexpr double RAD2DEG(180.f / M_PI);

struct fieldStats
{
  fieldStats() : min(-7000.0), max(2000.0) { range = max - min; }
  int gridsize;
  double min;
  double max;
  int nmiss;
  double missval;
  double mean;
  double var;
  double range;
  QString *varName;
  QString *varLongname;
  QString *varStdname;
  QString *varUnits;
};

struct gridStats
{
  int gridIsCircular;
  double *gridCenterLat;
  double *gridCenterLon;
  double *gridCornerLat;
  double *gridCornerLon;
  int ncorner;
  int *gridMask;
  int gridType;
};

class Field
{
    public:
  int m_vdate;
  int m_vtime;
  int m_streamID;
  int m_taxisID;

  int m_vlistID;
  int m_vlistID2;
  int m_gridID;
  int m_zaxisID;
  int m_nlevel;
  int m_levelID;
  int m_fileType;
  int m_varID;
  int m_tsID;
  double *m_field;
  fieldStats m_fieldStats;
  gridStats m_gridStats;
  Field();
  void fieldStat(double *p_field, fieldStats &p_field_stats);
  int open(const QString &p_fileName);
  void readField();
  void initField();
  void initGrid();
  int writeField(const char *filename);
  void printField();
  QPolygonF generatePoly(int p_varID, unsigned int p_fieldIDX, double p_scale);
};
#endif
