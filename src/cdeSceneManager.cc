#include "cdeSceneManager.h"
#include "cdePolygonMath.h"
#include "debugOutput.h"
// QT
#include "QGraphicsPixmapItem"

// c++ libs
#include <thread>

CdeSceneManager::CdeSceneManager(Field &p_field, ColorScheme &p_colorScheme,
                                 double &p_scale)
    : m_field(p_field), m_scale(p_scale)
{
  calculateSubSceneCnt(p_field.m_fieldStats.gridsize, 10000);
  initSubSections(p_field, p_colorScheme);
  generateSubScenes(m_field, 32, p_colorScheme);

  std::cout << "CdeSceneManager const done" << std::endl;
}

void
CdeSceneManager::init(Field &p_field, ColorScheme &p_colorScheme)
{
  std::cout << "CdeSceneManager init done" << std::endl;
}
void
CdeSceneManager::calculateSubSceneCnt(unsigned int p_cntPolys,
                                      unsigned int p_wanted)
{
  unsigned int numX = 2;
  unsigned int numY = 2;
  unsigned int curCntPer = p_cntPolys / (numX * numY);
  while (curCntPer > p_wanted)
    {
      numX += 4;
      numY += 2;
      curCntPer = (int) (p_cntPolys / (numX * numY));
    }
  Debuging(Debug::SCENEMANAGER, numX, " ", numY, " ", curCntPer);
  // print(curCntPer)
  // print(numX, " ", numY, " ", numX*numY)
  m_cntHorizontalSections = numX;
  m_cntVerticalSections = numY;
}

void
CdeSceneManager::initSubSections(Field &p_field, ColorScheme &p_colorScheme)
{

  int cntX = m_cntHorizontalSections;
  int cntY = m_cntVerticalSections;

  int subsectionWidht = m_scale * 360.f / cntX;

  m_subScenes = std::vector<std::vector<std::shared_ptr<CdeSubScene>>>(
      cntX, std::vector<std::shared_ptr<CdeSubScene>>(
                cntY, std::make_shared<CdeSubScene>(p_field, p_colorScheme,
                                                    m_scale)));

  // Not a good thing. TODO: find better alternative to init of subscene
  // vector
  int id = 0;
  for (auto &vec : m_subScenes)
    {
      for (auto &subScene : vec)
        {
          subScene
              = std::make_shared<CdeSubScene>(p_field, p_colorScheme, m_scale);
          subScene->m_id = id++;
        }
    }
}

std::shared_ptr<CdeSubScene>
CdeSceneManager::mapPolyToSubScene(QPolygonF &p_poly)
{
  return mapPointToSubScene(getPolygonCenter(p_poly));
}
std::shared_ptr<CdeSubScene>
CdeSceneManager::mapPointToSubScene(const QPointF &p_point)
{
  float lon = p_point.x();
  float lat = p_point.y();
  float subsectionWidhtFraction = m_cntHorizontalSections / 360.f;
  float subsectionHeightFraction = m_cntVerticalSections / 180.f;
  int x = std::floor((lon / (float) m_scale) * subsectionWidhtFraction);
  int y = std::floor((lat / (float) m_scale) * subsectionHeightFraction);

  /*
  if (x < 0 || y < 0)
    {
      std::cout << x << " " << y << std::endl;
      std::cout << lon << " " << lat << std::endl;
      std::cout << x << " " << y << std::endl;
    }
    */
  x = std::abs(m_cntHorizontalSections / 2 + x) % m_cntHorizontalSections;

  y = std::abs(y + m_cntVerticalSections / 2) % m_cntVerticalSections;
  return m_subScenes[x][y];
}

void
CdeSceneManager::generateSubScenes(Field &p_field, unsigned int p_cntSubScenes,
                                   ColorScheme &p_colorScheme)
{
  std::cout << "starting subScene generation" << std::endl;
  std::vector<std::thread> threads;
  unsigned int cntThreads = std::thread::hardware_concurrency();
  unsigned int cntPerThread = p_field.m_fieldStats.gridsize / cntThreads;
  unsigned int curStart = 0;
  unsigned int curEnd = curStart + cntPerThread;
  unsigned int rest
      = p_field.m_fieldStats.gridsize - (cntPerThread * cntThreads);
  if (rest > 0)
    {
      curEnd += rest;
    }
  for (unsigned int i = 0; i < cntThreads; i++)
    {
      std::cout << "started thread for scene gen: " << i << std::endl;
      threads.push_back(std::thread(&CdeSceneManager::addPolys, this,
                                    std::ref(p_field), curStart, curEnd));
      curStart = curEnd;
      curEnd += cntPerThread;
    }

  for (auto &curThread : threads)
    {
      std::cout << "waiting for thread: " << std::endl;
      curThread.join();
    }
  std::cout << "end subScene generation" << std::endl;
}

void
CdeSceneManager::addPolys(Field &p_field, unsigned int start, unsigned int end)
{
  for (unsigned i = start; i < end; i++)
    {
      addPolygon(i, p_field.generatePoly(0, i, m_scale));
    }
}

void
CdeSceneManager::addPolygon(int p_fieldEntryIdx, QPolygonF p_poly)
{
  auto subScene = mapPointToSubScene(getPolygonCenter(p_poly));
  subScene->add(p_fieldEntryIdx, p_poly);
}

void
CdeSceneManager::addSubscenes(QGraphicsScene *p_scene)
{
  int cntThreads = std::thread::hardware_concurrency();
  std::vector<std::thread> threads;
  for (int i = 0; i < m_cntHorizontalSections; i++)
    {
      for (int j = 0; j < m_cntVerticalSections; j += 4)
        {
          for (int k = 0; k < cntThreads && k + j < m_cntVerticalSections; k++)
            {
              threads.push_back(
                  std::thread(&CdeSubScene::initPixmap, m_subScenes[i][j + k]));
            }
          Debuging(Debug::SCENEMANAGER, "created batch of ", threads.size(),
                   " threads. Now waiting");

          for (auto &thr : threads)
            {
              thr.join();
            }
          Debuging(Debug::SCENEMANAGER, "finished Waiting");
          threads.clear();
        }
    }

  for (int i = 0; i < m_cntHorizontalSections; i++)
    {
      for (int j = 0; j < m_cntVerticalSections; j++)
        {
          auto pixmap = m_subScenes[i][j]->addToScene(p_scene);
        }
    }
}

void
CdeSceneManager::updateColors(ColorScheme &p_colorScheme)
{
  for (auto &subSceneRow : m_subScenes)
    {
      for (auto &subScene : subSceneRow)
        {
          subScene->updateColors(p_colorScheme);
        }
    }
}
