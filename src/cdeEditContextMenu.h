#ifndef CDEEDITCONTEXTMENU_H
#define CDEEDITCONTEXTMENU_H

#include <QMenu>
#include "cdeGraphicsView.h"

class CdeEditContextMenu : public QMenu
{
  Q_OBJECT
private:
    double result;
public:
  CdeEditContextMenu(QWidget *p_parent);
  double getResult();
  void openEditWindow();
};
#endif
