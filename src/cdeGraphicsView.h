
#ifndef CDEGRAPHICSVIEW_HPP
#define CDEGRAPHICSVIEW_HPP

#include <QGraphicsPolygonItem>
#include <QGraphicsView>
#include <QWheelEvent>
#include <QRubberBand>
#include <QLabel>

#include <iostream>
#include <set>
#include <stack>
#include <thread>
#include <memory>

#include "field.h"
#include "../colors/colorScheme.h"
#include "cdeSceneManager.h"

struct CellLabel
{
  QGraphicsScene *m_sceneRef;
  QGraphicsRectItem *m_rect;
  QGraphicsLineItem *m_firstLine;
  QGraphicsLineItem *m_secondLine;
  QGraphicsTextItem *m_text;
};

class CdeGraphicsView : public QGraphicsView
{
  Q_OBJECT

public:
  Field m_field;

  float m_curAngle = 0.0;

  ColorScheme m_colorScheme;

  QGraphicsScene *m_scene;
  QPixmap *m_pixmap;
  QGraphicsPixmapItem *m_pixmapItem;

  QRubberBand *m_rubberBand = nullptr;
  bool m_mousePressed = false;
  QPoint m_rubberBandOrigin;

  double defaultPenSize = 1;


  std::vector<QGraphicsPolygonItem *> m_selectionPolys;
  std::map<unsigned int, QGraphicsPolygonItem*> m_selections;
  std::map<int, CellLabel> m_displayedLabels;
  std::set<unsigned int> m_selectedEntriesIdx;

  // pair = <oldValue, polyThatShows the Change>
  std::stack<std::pair<double, QGraphicsPolygonItem>> m_changedPolys;

  double m_maxHorizontal = 0;
  double m_maxVertical = 0;
  double m_minHorizontal = 0;
  double m_minVertical = 0;

  double m_scale = 80;


  //Thrading


  // Functions
  CdeGraphicsView(const QString p_fileName,QWidget *p_parent = nullptr);

  bool eventFilter(QObject *object, QEvent *event);
  void wheelEvent(QWheelEvent *event);
  void mouseReleaseEvent(QMouseEvent *p_event);
  void mousePressEvent(QMouseEvent *p_event);
  void mouseMoveEvent(QMouseEvent *p_event);
  void keyPressEvent(QKeyEvent *p_event);


  int initScene(QString p_fileName);
 // void initImage();

  std::unique_ptr<CdeSceneManager> m_manager;

  void handleSelectionChange(int p_fieldEntryIdx);
  void drawSelectionPolygon(int p_fieldEntryIdx);

  void handleEdit(double p_newValue);
  void writeField(std::string p_fileName);
  std::vector<QPolygonF> test(int i, int k);
  void buildCellLabel(int p_ID, QPolygonF &p_poly);

public slots:
  void updateColors(double p_min, double p_max);
  //void updateRenderState(std::thread::id);

signals:
  void progressUpdate(std::thread::id, int);
  void renderThreadDone(std::thread::id);
};

#endif
