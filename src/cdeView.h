#include "cdeCheckBox.h"
#include "ui/ui_View.h"
#include <QDockWidget>
#include <QtWidgets/QLabel>
#include "field.h"

#include "cdeGraphicsView.h"
#include <map>

class CdeView : public QDockWidget
{
  Q_OBJECT
public:
  CdeView(QString p_name, int p_ID, QWidget *p_parent);
  void addConnectionTickBox(int viewCount, int activeViewsCnt);
  void updateCoordinates(double p_lon, double p_lat);
  void updateColorRange(int p_min, int p_max);
  void setCheckBox(int p_boxID, bool state);
  void saveField(std::string p_fileName);
  void saveField();
  int
  getID()
  {
    return m_ID;
  };
  bool isConnectedTo(int p_dest);

private:
  Ui::View m_ui;
  QWidget *m_centralWidget;
  int m_ID;
  std::map<int, CdeCheckBox *> m_checkBoxes;

  std::string m_fileName;
  CdeGraphicsView *m_view;
  ColorScheme m_colorScheme;

  void mouseReleaseEvent(QMouseEvent *p_event);
  void setQtConnections(QWidget *p_parent);
  bool eventFilter(QObject *object, QEvent *event);
public:
  void setSelected(bool p_selected);
  void updateColors(double p_min, double p_max);

private slots:
  void handleDistantConnectedUpdate();
  void handle_addConnectionTickBox(int viewCount, int activeViewsCnt);
  void removeConnectionTickBox(int viewID);

  void closeEvent(QCloseEvent *);
  void handle_viewVerticalChange(int);
  void handle_viewHorizontalChange(int);
  void handle_viewUpdate(int, int);
  void handle_connectionChange(int, int, bool);

signals:
  void gotSelected(CdeView *p_selected);
  void viewClosed();
  void viewChanged(int, int);
  void connectionAdded(int, int, bool);
  void connectionRemoved(int, int, bool);
};
