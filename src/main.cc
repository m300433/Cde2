
#include <QApplication>
#include "cdeMainWindow.h"

#include "cdeOptions.h"
int
main(int argc, char **argv)
{

  CdeOptions::parse(argc, argv);
  CdeOptions::print();
  QApplication app(argc, argv);

  CdeMainWindow *widget = new CdeMainWindow();
  widget->show();

  return app.exec();
}
