#ifndef CDECHECK_BOX
#define CDECHECK_BOX

#include <QCheckBox>

class CdeCheckBox : public QCheckBox
{
  Q_OBJECT
public:
  CdeCheckBox(QString p_name, int p_ID);

private:
  const int m_ID;
public slots:
  void handle_stateChanged(int);
signals:
  void stateChanged(int state, int p_id, bool both);
};

#endif
