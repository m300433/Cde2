#include "field.h"
#include <iostream>
#include <QString>
#include "../cdilib/grid.h"

struct vlist_t;

Field::Field()
{
  m_vdate = -1;
  m_vtime = -1;
  m_streamID = -1;
  m_taxisID = -1;

  m_vlistID = -1;
  m_vlistID2 = -1;
  m_gridID = -1;
  m_zaxisID = -1;
  m_nlevel = -1;
  m_levelID = -1;
  m_fileType = -1;
  m_varID = -1;
  m_tsID = -1;
}

void
Field::fieldStat(double *p_field, fieldStats &p_field_stats)
{
  std::cout << "Start fieldStat" << std::endl;
  int i, ivals, imiss = 0;
  double arrmin, arrmax, arrmean, arrvar;

  if (p_field_stats.nmiss > 0)
    {
      ivals = 0;
      arrmean = 0;
      arrvar = 0;
      arrmin = 1e50;
      arrmax = -1e50;
      for (i = 0; i < p_field_stats.gridsize; i++)
        {
          if (!DBL_IS_EQUAL(p_field[i], p_field_stats.missval))
            {
              if (p_field[i] < arrmin) arrmin = p_field[i];
              if (p_field[i] > arrmax) arrmax = p_field[i];
              arrmean += p_field[i];
              arrvar += p_field[i] * p_field[i];
              ivals++;
            }
        }
      imiss = p_field_stats.gridsize - ivals;
      p_field_stats.gridsize = ivals;
    }
  else
    {
      arrmean = p_field[0];
      arrvar = p_field[0];
      arrmin = p_field[0];
      arrmax = p_field[0];
      for (i = 1; i < p_field_stats.gridsize; i++)
        {
          if (p_field[i] < arrmin) arrmin = p_field[i];
          if (p_field[i] > arrmax) arrmax = p_field[i];
          arrmean += p_field[i];
          arrvar += p_field[i] * p_field[i];
        }
    }

  if (p_field_stats.gridsize)
    {
      arrmean = arrmean / p_field_stats.gridsize;
      arrvar = arrvar / p_field_stats.gridsize - arrmean * arrmean;
    }

  if (imiss != p_field_stats.nmiss && p_field_stats.nmiss > 0)
    fprintf(stdout, "Found %d of %d missing values!\n", imiss,
            p_field_stats.nmiss);

  p_field_stats.min = arrmin;
  p_field_stats.max = arrmax;
  p_field_stats.mean = arrmean;
  p_field_stats.var = arrvar;
  p_field_stats.range = arrmax - arrmin;
  std::cout << "End fieldStat" << std::endl;
}

int
Field::open(const QString &p_fileName)
{
  QByteArray afilename = p_fileName.toLatin1();
  const char *fname = afilename.constData();

  m_streamID = streamOpenRead(fname);
  if (m_streamID < 0)
    {

      std::cout << "Error while opening file: " << fname << std::endl;
      return -1;
      /*TEMP*/
      // openErrorMessageDialog->showMessage(
      //   QString("Open failed on '%1'").arg(p_fileName));
      // quit application !!!
    }
  else
    {
      m_fileType = streamInqFiletype(m_streamID);
      qDebug("Open file: %s", p_fileName);

      initField();
      readField();

      /*TEMP*/  // NEEDS TO GO TO VIEW
                // m_color_scheme.get_ranges() =
      // create_splitted_linear_distribution(m_color_scheme.size(),
      // m_fieldStats);  update_colors();
      // view->set_start_range(m_fieldStats.min, m_fieldStats.max);
      // setCentralWidget(view);
      return 0;
    }
}
int
Field::writeField(const char *filename)
{
  int streamID;
  int varID = 0, levelID = 0, tsID = 0;
  int status = 0;
  int nmiss;

  std::cout << "FilenameWF: " << filename << std::endl;
  streamID = streamOpenWrite(filename, m_fileType);
  if (streamID < 0)
    {
      std::cout << "test" << __LINE__ << std::endl;
      fprintf(stderr, "%s\n", cdiStringError(streamID));
      std::cout << "test" << __LINE__ << std::endl;
      return (1);
    }
  else
    {
      std::cout << "test" << __LINE__ << std::endl;
      streamDefVlist(streamID, m_vlistID2);

      taxisDefVdate(m_taxisID, m_vdate);
      taxisDefVtime(m_taxisID, m_vtime);

      qDebug() << "writeField: ";
      streamDefTimestep(streamID, tsID);

      nmiss = 0;
      for (int i = 0; i < m_fieldStats.gridsize; ++i)
        if (DBL_IS_EQUAL(m_field[i], m_fieldStats.missval)) nmiss++;

      streamWriteVar(streamID, varID, m_field, nmiss);

      streamClose(streamID);
    }

  return (status);
}

void
Field::readField()
{
  m_varID = 0;
  m_levelID = 0;
  m_tsID = 0;

  qDebug() << "readField: ";

  int nrecs;
  size_t size;

  nrecs = streamInqTimestep(m_streamID, m_tsID);
  if (nrecs == 0) qFatal("Reading of timestep %d failed!\n", m_tsID);

  m_vdate = taxisInqVdate(m_taxisID);
  m_vtime = taxisInqVtime(m_taxisID);

  size = m_fieldStats.gridsize * sizeof(double);

  std::cout << size << std::endl;
  m_field = (double *) malloc(size);

  if (m_field == NULL) qFatal("No memory!");

  // field is set in this function (among others?)
  std::cout << "streamReadVarSlice" << std::endl;
  streamReadVarSlice(m_streamID, m_varID, m_levelID, m_field,
                     &m_fieldStats.nmiss);

  // sets m_fieldStats
  std::cout << "fieldStats" << std::endl;
  fieldStat(m_field, m_fieldStats);

  std::cout << "fieldStat: " << m_fieldStats.min << m_fieldStats.max
            << m_fieldStats.mean << m_fieldStats.var
            << (m_fieldStats.max - m_fieldStats.min)
            << (m_fieldStats.max - m_fieldStats.min) / m_fieldStats.var
            << std::endl;
}
void
Field::initField(void)
{
  std::cout << "Start inigField" << std::endl;
  char buffer[65536];
  int varID = 0;

  m_vlistID = streamInqVlist(m_streamID);

  std::cout << vlistNvars(m_vlistID) << std::endl;
  char name[100];
  for (int i = 0; i < vlistNvars(m_vlistID); i++)
    {
      vlistInqVarName(m_vlistID, i, name);
      printf("%s\n", name);
    }
  vlistClearFlag(m_vlistID);
  vlistDefFlag(m_vlistID, 0, 0, 1);
  m_vlistID2 = vlistCreate();
  vlistCopyFlag(m_vlistID2, m_vlistID);

  m_taxisID = vlistInqTaxis(m_vlistID);

  buffer[0] = 0;
  vlistInqVarName(m_vlistID, varID, buffer);
  m_fieldStats.varName = new QString(buffer);

  buffer[0] = 0;
  vlistInqVarLongname(m_vlistID, varID, buffer);
  if (buffer[0])
    m_fieldStats.varLongname = new QString(buffer);
  else
    m_fieldStats.varLongname = NULL;

  buffer[0] = 0;
  vlistInqVarStdname(m_vlistID, varID, buffer);
  if (buffer[0])
    m_fieldStats.varStdname = new QString(buffer);
  else
    m_fieldStats.varStdname = NULL;

  buffer[0] = 0;
  vlistInqVarUnits(m_vlistID, varID, buffer);
  if (buffer[0])
    m_fieldStats.varUnits = new QString(buffer);
  else
    m_fieldStats.varUnits = NULL;

  m_fieldStats.missval = vlistInqVarMissval(m_vlistID, varID);

  m_gridID = vlistInqVarGrid(m_vlistID, varID);
  m_fieldStats.gridsize = gridInqSize(m_gridID);
  m_zaxisID = vlistInqVarZaxis(m_vlistID, varID);
  m_nlevel = zaxisInqSize(m_zaxisID);

  m_gridStats.gridType = gridInqType(m_gridID);

  initGrid();
  std::cout << "End initField" << std::endl;
}

void
Field::initGrid(void)
{
  std::cout << "Start initGrid" << std::endl;
  char units[128];
  int nlon, nlat;
  int nalloc;
  int i;
  int lgrid_gen_bounds = 0;

  if (gridInqType(m_gridID) != GRID_LONLAT
      && gridInqType(m_gridID) != GRID_GAUSSIAN
      && gridInqType(m_gridID) != GRID_GME
      && gridInqType(m_gridID) != GRID_CURVILINEAR
      && gridInqType(m_gridID) != GRID_UNSTRUCTURED)
    qFatal("Output of %s data failed!", gridNamePtr(gridInqType(m_gridID)));

  if (gridInqType(m_gridID) != GRID_UNSTRUCTURED
      && gridInqType(m_gridID) != GRID_CURVILINEAR)
    {
      if (gridInqType(m_gridID) == GRID_GME)
        {
          m_gridID = gridToUnstructured(m_gridID);
          m_gridStats.gridMask
              = (int *) malloc(m_fieldStats.gridsize * sizeof(int));
          gridInqMask(m_gridID, m_gridStats.gridMask);
        }
      else
        {
          m_gridID = gridToCurvilinear(m_gridID);
          lgrid_gen_bounds = 1;
        }
    }
  /*
  nlon     = gridInqXsize(m_gridID);
  nlat     = gridInqYsize(m_gridID);

  qDebug("nlon = %d   nlat = %d", nlon, nlat);
  */
  if (gridInqType(m_gridID) == GRID_UNSTRUCTURED)
    m_gridStats.ncorner = gridInqNvertex(m_gridID);
  else
    m_gridStats.ncorner = 4;

  qDebug("m_gridStats.ncorner: %d", m_gridStats.ncorner);

  m_gridStats.gridIsCircular = gridIsCircular(m_gridID);

  m_gridStats.gridCenterLat
      = (double *) malloc(m_fieldStats.gridsize * sizeof(double));
  m_gridStats.gridCenterLon
      = (double *) malloc(m_fieldStats.gridsize * sizeof(double));

  gridInqYvals(m_gridID, m_gridStats.gridCenterLat);
  gridInqXvals(m_gridID, m_gridStats.gridCenterLon);
  std::cout << "============================= " << gridInqXsize(m_gridID) << " "
            << gridInqYsize(m_gridID) << " " << gridInqSize(m_gridID)
            << std::endl;

  /* Convert lat/lon units if required */

  gridInqYunits(m_gridID, units);

  if (strncmp(units, "radian", 6) == 0)
    {
      for (i = 0; i < m_fieldStats.gridsize; i++)
        {
          m_gridStats.gridCenterLat[i] *= RAD2DEG;
          m_gridStats.gridCenterLon[i] *= RAD2DEG;
        }
    }
  else if (strncmp(units, "degrees", 7) == 0)
    {
      /* No conversion necessary */
    }
  else
    {
      qDebug("Unknown units (%s) supplied for grid1 center lat/lon: "
             "proceeding assuming degrees",
             units);
    }

  //  if ( luse_grid_corner )
  {
    if (m_gridStats.ncorner == 0) qFatal("grid corner missing!");
    nalloc = m_gridStats.ncorner * m_fieldStats.gridsize;
    m_gridStats.gridCornerLat = (double *) malloc(nalloc * sizeof(double));
    m_gridStats.gridCornerLon = (double *) malloc(nalloc * sizeof(double));

    if (gridInqYbounds(m_gridID, NULL) && gridInqXbounds(m_gridID, NULL))
      {
        gridInqYbounds(m_gridID, m_gridStats.gridCornerLat);
        gridInqXbounds(m_gridID, m_gridStats.gridCornerLon);
      }
    else
      {
        /*
        if ( lgrid_gen_bounds )
          {
            genXbounds(nlon, nlat, m_gridStats.gridCenterLon,
        m_gridStats.gridCornerLon); genYbounds(nlon, nlat,
        m_gridStats.gridCenterLat, m_gridStats.gridCornerLat);
          }
        else
        */
        qFatal("Grid corner missing!");
      }

    if (strncmp(units, "radian", 6) == 0)
      {
        /* Note: using units from latitude instead from bounds */
        for (i = 0; i < m_gridStats.ncorner * m_fieldStats.gridsize; i++)
          {
            m_gridStats.gridCornerLat[i] *= RAD2DEG;
            m_gridStats.gridCornerLon[i] *= RAD2DEG;
          }
      }
    else if (strncmp(units, "degrees", 7) == 0)
      {
        /* No conversion necessary */
      }
    else
      {
        qDebug("Unknown units (%s) supplied for grid1 center lat/lon: "
               "proceeding assuming degrees",
               units);
      }
  }
  std::cout << "End initGrid" << std::endl;
}
QPolygonF
Field::generatePoly(int variable, unsigned int p_polyID, double scale)
{
  int ncorner = m_gridStats.ncorner;
  double val;
  int k;
  double lon_bounds[ncorner + 1], lat_bounds[ncorner + 1];
  double lon, lat;

  double x;
  double y;

  val = m_field[p_polyID];
  lon = m_gridStats.gridCenterLon[p_polyID];
  lat = m_gridStats.gridCenterLat[p_polyID];

  QPolygonF polygon;
  for (k = 0; k < ncorner; ++k)
    {
      lon_bounds[k] = m_gridStats.gridCornerLon[p_polyID * ncorner + k];
      lat_bounds[k] = m_gridStats.gridCornerLat[p_polyID * ncorner + k];
    }

  for (k = 0; k < ncorner; ++k)
    {
      if ((lon - lon_bounds[k]) > 270) lon_bounds[k] += 360;
      if ((lon_bounds[k] - lon) > 270) lon_bounds[k] -= 360;
    }

  for (k = 0; k < ncorner; k++)
    {

      x = scale * lon_bounds[k % ncorner];
      y = scale * -1 * lat_bounds[k % ncorner];

      polygon << QPointF(x, y);
    }
  return polygon;
}

void
Field::printField()
{
  std::cout << "m_vdate    " << m_vdate << std::endl;
  std::cout << "m_vtime    " << m_vtime << std::endl;
  std::cout << "m_streamID " << m_streamID << std::endl;
  std::cout << "m_taxisID  " << m_taxisID << std::endl;
  std::cout << "m_vlistID  " << m_vlistID << std::endl;
  std::cout << "m_vlistID2 " << m_vlistID2 << std::endl;
  std::cout << "m_gridID   " << m_gridID << std::endl;
  std::cout << "m_zaxisID  " << m_zaxisID << std::endl;
  std::cout << "m_nlevel   " << m_nlevel << std::endl;
  std::cout << "m_levelID  " << m_levelID << std::endl;
  std::cout << "m_fileType " << m_fileType << std::endl;
  std::cout << "m_varID    " << m_varID << std::endl;
  std::cout << "m_tsID     " << m_tsID << std::endl;
}
