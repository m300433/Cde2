// c++ std
#include <thread>
#include <vector>
#include <map>
#include <set>

// Qt
#include <QImage>
#include <QPolygonF>
#include <QPen>
#include <QProgressDialog>

// cde
#include "field.h"

// colors
#include "../colors/colorScheme.h"
#include "../colors/colorDistributionFunctions.h"

class CdeImageGenerator : public QObject
{
  Q_OBJECT

public:
  CdeImageGenerator(QImage &p_image, std::vector<QPolygonF> &p_polys,
                    Field &p_field, ColorScheme &p_colorScheme);

private:
  std::vector<QPolygonF> &m_polys;
  QImage &m_image;
  Field &m_field;
  ColorScheme &m_colorScheme;

  QPen m_pen;

  std::map<std::thread::id, QImage *> m_subImages;
  std::map<int, std::thread::id> m_threadIds;
  std::map<std::thread::id, int> m_threadStatus;
  QProgressDialog m_progressDiag;
  std::set<std::thread::id> m_finishedThreads;
  int m_numThreads;

  void initProgressDialog();
  void finalizeImage();
  void generateImage(int p_startIdx, int p_endIdx);
  void run();

public slots:
  void updateProgressBar(std::thread::id p_thrID, int p_status);
  void handleThreadFinished(std::thread::id);

signals:
  void progressUpdate(std::thread::id p_thrID, int p_status);
  void threadFinished(std::thread::id p_thrID);
  void imageDone(QImage &p_image);
};
