#include "colorScheme.h"
#include "../src/field.h"
#include <QColor>
#include <vector>

ColorScheme::ColorScheme(std::vector<double> p1, std::vector<std::vector<int>> p2) {
    m_schemeColors = p2;
    m_start_color_ranges = p1;
}
std::vector<std::vector<int>> &ColorScheme::get_colors() {
    return m_schemeColors;
}
std::vector<double> &ColorScheme::get_ranges() {
    return m_start_color_ranges;
}

unsigned long ColorScheme::size() {
    return m_schemeColors.size();
}
/*
void ColorScheme::save(cereal::XMLOutputArchive &archive) const {
    archive(m_schemeColors, m_start_color_ranges);
}
void ColorScheme::load(cereal::XMLInputArchive &archive) {
    archive(m_schemeColors, m_start_color_ranges);
}
*/
QColor ColorScheme::getColorFromValue(double p_value) {
    unsigned long color_index = 0;
   
    while (color_index < (m_schemeColors.size() - 1) &&
           m_start_color_ranges[color_index] < p_value) {
        color_index++;
    }
    return QColor(m_schemeColors[color_index][0], m_schemeColors[color_index][1], m_schemeColors[color_index][2]);
}
