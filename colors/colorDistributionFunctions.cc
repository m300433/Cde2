#include "colorScheme.h"
#include "colorDistributionFunctions.h"
#include "../src/field.h"
#include <iostream>
#include <vector>
#include <math.h>
std::vector<double>
create_linear_distribution(unsigned long p_cnt_colors, fieldStats p_data_limits)
{
  std::vector<double> color_distribution;
  double step = p_data_limits.range / (float) p_cnt_colors;
  for (double i = p_data_limits.min; i < 0; i += step)
    {
      color_distribution.push_back(i);
    }
  return color_distribution;
}
/*
std::vector<double> create_splitted_linear_distribution(unsigned long
p_cnt_colors, double p_min, double p_max) { p_cnt_colors -= 2;
    std::vector<double> color_distribution;
    double cnt_sub_zero = std::ceil(p_cnt_colors/2);
    double cnt_above_zero = std::floor(p_cnt_colors/2);
    double step_sub_zero = p_min / cnt_sub_zero;
    double step_above_zero = p_max / cnt_above_zero;
    for (int i = cnt_sub_zero  ; i > 0; i--) {
        color_distribution.push_back(step_sub_zero * i);
    }
    color_distribution.push_back(0);
    for (int i = 1; i < cnt_above_zero + 1; i++) {
        color_distribution.push_back(step_above_zero * i);
    }
    color_distribution.push_back(p_max*2);
    std::cout << "Size colors dist: " << color_distribution.size() << " " <<
p_cnt_colors << std::endl; return color_distribution;
}
*/

std::vector<double>
create_splitted_linear_distribution(unsigned long p_cnt_colors, double p_min,
                                    double p_max, bool relativeColors)
{
  p_cnt_colors -= 2;
  std::vector<double> color_distribution(p_cnt_colors + 2);
  double cnt_sub_zero = std::ceil(p_cnt_colors / 2);
  double cnt_above_zero = std::floor(p_cnt_colors / 2);
  double step_sub_zero = std::abs(std::min(0.0, p_min / cnt_sub_zero));
  double step_above_zero;
  if (step_sub_zero > 0)
    {
      step_above_zero = p_max / cnt_above_zero;
    }
  else
    {
      step_above_zero = (p_max - p_min) / cnt_above_zero;
      std::cout << "stepABZ" << step_above_zero << std::endl;
    }
  std::cout << cnt_sub_zero << " " << step_sub_zero << " | " << cnt_above_zero
            << " " << step_above_zero << std::endl;
  int i = 0;
  for (; i < cnt_sub_zero; i++)
    {
      color_distribution[i] = (p_min + (step_sub_zero * i));
      std::cout << "colDistNeg " << i << " " << color_distribution[i]
                << std::endl;
    }
  color_distribution[i++] = (0);
  for (; i < p_cnt_colors - 2; i++)
    {
      if (relativeColors)
        {
          color_distribution[i]
              = std::max(p_min, 0.0) + (step_above_zero * (i - cnt_sub_zero));
        }
      else
        {
          color_distribution[i] = (step_above_zero * (i - cnt_sub_zero));
        }
      std::cout << "colDistPos " << i << " " << color_distribution[i]
                << std::endl;
    }
  color_distribution[i] = (p_max);
  std::cout << "Size colors dist: " << color_distribution.size() << " "
            << p_cnt_colors << std::endl;
  return color_distribution;
}
