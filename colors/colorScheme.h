#ifndef COLOR_SCHEME_H
#define COLOR_SCHEME_H

#include <QColor>
#include <map>
#include <memory>
#include <vector>
class ColorScheme {
  private:
    std::vector<double> m_start_color_ranges;
    std::vector<std::vector<int>> m_schemeColors;

  public:
    ColorScheme() {
    }
    ColorScheme(std::vector<double> p_colorStartValues, std::vector<std::vector<int>> p_schemeColors);
    std::vector<std::vector<int>> &get_colors();
    std::vector<double> &get_ranges();
    unsigned long size();
    QColor getColorFromValue(double p_value);
};
#endif
