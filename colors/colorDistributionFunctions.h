#ifndef COLOR_DISTRIBUTION_FUNCTIONS_H
#define COLOR_DISTRIBUTION_FUNCTIONS_H
#include "colorScheme.h"
#include "../src/field.h"
#include <vector>

std::vector<double> create_linear_distribution(unsigned long p_cnt_colors, fieldStats p_data_limts);
std::vector<double> create_splitted_linear_distribution(unsigned long p_cnt_colors,
                                                        double p_min, double p_max, bool relativeColors = true);

#endif
